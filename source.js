"use strict";

/////////////////////////////////////////
// Name-Analysis-Results function

document.querySelector(".check").addEventListener("click", function () {
  let name = document.querySelector(".nameInput").value;
  console.log(name);
  console.log(typeof name);
  document.querySelector(".score").textContent = name.length;
  let allCharacters = {};
  for (let i = 0; i < name.length; i++) {
    if (name[i] in allCharacters) {
      allCharacters[name[i]]++;
    } else {
      allCharacters[name[i]] = 1;
    }
  }
  document.querySelector(".show-all-characters").textContent = JSON.stringify(allCharacters);
});

////////////////////////////////////////
// Refreshing Enter-Name-field &  Name-Analysis-Results

function clearInput() {
  document.querySelector(".nameInput").value = "";
  document.querySelector(".score").textContent = "0";
  document.querySelector(".show-all-characters").textContent = "";
}

